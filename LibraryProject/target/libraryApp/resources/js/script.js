var HOME = "/libraryApp";
/***************************************/
/*******      Обработчики       ********/
/***************************************/

/**
 * Сохранение книги
 */
$( "#myModal" ).find( "#save" ).click(function() {
    var elements = ["#isn", "#author", "#bookName"];
    removeCssClass("#myModal", elements, "error");
    $("#myModal").find("#status").addClass("hidden");
    var isn = $("#isn").val();
    var author = $("#author").val();
    var bookName = $("#bookName").val();

    if (isValid(isn) && isValid(author) && isValid(bookName)) {
        var msg = {};
        msg["isn"] = isn;
        msg["author"] = author;
        msg["bookName"] = bookName;
        msg["owner"] = "null";

        function success(data) {
            console.log("SUCCESS: ", data["status"]);
            $("#myModal").find("#status").text(data["status"]);
            $("#myModal").find("#status").removeClass("hidden");
        }
        sendData(msg, "POST", HOME +"/api/saveBook", success);

    } else {
        if (!isValid(isn)) {
            $("#isn").addClass("error");
        }
        if (!isValid(author)) {
            $("#author").addClass("error");
        }
        if (!isValid(bookName)) {
            $("#bookName").addClass("error");
        }
    }
});

/**
 * Сохранение пользователя
 */
$( "#addUserModal" ).find( "#save" ).click(function() {

    var elements = ["#name", "#password"];

    removeCssClass("#addUserModal", elements, "error");
    $("#addUserModal").find("#status").addClass("hidden");
    var name =  $("#addUserModal").find("#name").val();
    var password =  $("#addUserModal").find("#password").val();

    if (isValid(name) && isValid(password)) {
        var msg = {};
        msg["name"] = name;
        msg["password"] = password;

        function success(data) {
            console.log("SUCCESS: ", data["status"]);
            $("#addUserModal").find("#status").text(data["status"]);
            $("#addUserModal").find("#status").removeClass("hidden");
        }
        sendData(msg, "POST", HOME + "/api/saveUser", success);

    } else {
        if (!isValid(name)) {
            $("#addUserModal").find("#name").addClass("error");
        }
        if (!isValid(password)) {
            $("#addUserModal").find("#password").addClass("error");
        }
    }
});

/**
 * Обновление книги
 */
$( "#updateBookModal" ).find("#update").click(function() {
    var elements = ["#author", "#bookName"];
    removeCssClass("#updateBookModal", elements, "error");
    $("#updateBookModal").find("#status").addClass("hidden");
    var isn = $("#updateBookModal").find("#isn").text();
    var author = $("#updateBookModal").find("#author").val();
    var bookName = $("#updateBookModal").find("#bookName").val();

    if (isValid(author) && isValid(bookName)) {
        var msg = {};
        msg["isn"] = isn;
        msg["author"] = author;
        msg["bookName"] = bookName;

        function success(data) {
            console.log("SUCCESS: ", data);
            $("#updateBookModal").find("#status").text(data["status"]);
            $("#updateBookModal").find("#status").removeClass("hidden");
        }
        sendData(msg, "POST", HOME + "/api/updateBook", success);

    } else{
        if (!isValid(author)) {
            $("#updateBookModal").find("#author").addClass("error");
        }
        if (!isValid(bookName)) {
            $("#updateBookModal").find("#bookName").addClass("error");
        }
    }
});

/**
 * перезагрузка страницы после закрытия модального окна
 */
$('#updateBookModal').on('hidden.bs.modal', function () {
    location.reload();
});
$('#myModal').on('hidden.bs.modal', function () {
    location.reload();
});
$('#addUserModal').on('hidden.bs.modal', function () {
    location.reload();
});
$('#updateUserModal').on('hidden.bs.modal', function () {
    location.reload();
});

/**
 * Обновление пользователя
 */
$( "#updateUserModal" ).find("#update").click(function() {
    var elements = ["#password"];
    removeCssClass("#updateUserModal", elements, "error");
    $("#updateUserModal").find("#status").addClass("hidden");

    var name = $("#updateUserModal").find("#name").text();
    var password = $("#updateUserModal").find("#password").val();

    if (isValid(password)) {
        var msg = {};
        msg["name"] = name;
        msg["password"] = password;

        function success(data) {
           console.log("SUCCESS: ", data);
            $("#updateUserModal").find("#status").text(data["status"]);
            $("#updateUserModal").find("#status").removeClass("hidden");
        }
        sendData(msg, "POST", HOME + "/api/updateUser", success);

    } else{
        $("#updateUserModal").find("#password").addClass("error");
    }
});

/**
 * Показать еще больше книг
 */
$("#showMore").click(function() {
    var rowLength = $("#booksTable > tbody > tr").length;
    var sortColumn = $("#booksTable").find("a.sort-column").first();

    var msg = {};
    msg["rowLength"] = rowLength;
    msg["msg"] = sortColumn.attr("data-sortby");
    msg["orderBy"] = sortColumn.attr("data-order");

    function success(data) {
        $("#booksTable > tbody > tr").last().after(getBooksView(data));
    }

    sendData(msg, "POST", HOME + "/api/getMore", success);
});


/***************************************/
/*******   Служебные функции    ********/
/***************************************/

/**
 * Отправка сообщения
 * @param msg данные для отправки
 * @param type http метод
 * @param url адрес отправки
 * @param successFun функция, вызываемая при успешной отправке данных
 */
function sendData(msg, type, url, successFun) {
    $.ajax({
        type: type,
        url: url,
        data: msg, // parameters
        success : function(data) {
            successFun(data);
        },
        error : function(e) {
            console.log("ERROR: ", e);
        },
        done : function(e) {
            console.log("DONE");
        }
    })
}

/**
 * удаление класса для всех указанных элементов
 * @param modalId
 * @param className
 */
function removeCssClass(modalId, elements, className) {
    for (e in elements) {
        $(modalId).find(elements[e]).removeClass(className);
    }
}

/**
 * html отображение книг
 * @param data данные о книгах
 * @returns {string} html отображение
 */
function getBooksView(data) {
    var result = "";
    for (var i in data) {
        var book = $.parseJSON(data[i]);
        var dataBookParams = book.isn + "','" + book.author + "','" + book.bookName;
        var a = "<a href='#updateBookModal' onclick=\"getDataBook("+ "\'" + dataBookParams + "\'" +")\" data-toggle='modal'>"+ book.isn +"</a>";
        var td = "<td>" + a + "</td><td>" + book.author +"</td><td>" + book.bookName + "</td>";
        var buttonParams = "id=\'remove-" + book.isn + "\' onClick=\"removeBook("+ "\'" + book.isn + "\'" +")\"";
        var removeBtn;
        if (book.owner === "null") {
            removeBtn = "<td><button "+ buttonParams +">Удалить</button></td>";
        } else {
            removeBtn = "<td>У книги есть владелец</td>";
        }

        var owner = ownerView(book.owner, book.isn);
        result += "<tr>" + td + "<td>" + owner + "</td>" + removeBtn + "</tr>";
    }
    return result;
}

/**
 * Сортировка по столбцу
 * @param param столбец
 */
function sortBook(param) {
    var element;
    if (param === "author") {
        element = $("#booksTable").find("#authorSort");
    } else if (param === "bookName") {
        element = $("#booksTable").find("#bookNameSort");
    }
    $("#booksTable").find("a").removeClass("sort-column");
    element.addClass("sort-column");
    var orderBy = element.attr("data-order");
    reverseOrderBy(element, orderBy);
    orderBy = element.attr("data-order");

    var msg = {};
    msg["msg"] = param;
    msg["orderBy"] = orderBy;

    function success(data) {
        console.log("sortBook SUCCESS: ", data);
        var res = generateTbodyContent(data);
        $("#booksTable > tbody").html(res);
    }
    sendData(msg, "POST", HOME +"/api/sortBook", success);

}

/**
 * генерирование содержимого tBody, для списка книг
 * @param data входные данные
 * @returns {string} сгенерированные строки
 */
function generateTbodyContent(data) {
    var result;
    result = "";

    for (var i in data) {
        var book = $.parseJSON(data[i]);
        var dataBookParams = book.isn + "','" + book.author + "','" + book.bookName;
        var a = "<a href='#updateBookModal' onclick=\"getDataBook("+ "\'" + dataBookParams + "\'" +")\" data-toggle='modal'>"+ book.isn +"</a>";
        var td = "<td>" + a + "</td><td>" + book.author +"</td><td>" + book.bookName + "</td>";
        var buttonParams = "id=\'remove-" + book.isn + "\' onClick=\"removeBook("+ "\'" + book.isn + "\'" +")\"";
        var removeBtn;
        if (book.owner === "null") {
            removeBtn = "<td><button "+ buttonParams +">Удалить</button></td>";
        } else {
            removeBtn = "<td>У книги есть владелец</td>";
        }
        var owner = ownerView(book.owner, book.isn);
        result +="<tr>" + td + "<td>" + owner + "</td>" + removeBtn + "</tr>";
    }
    return result;
}

/**
 * изменение порядка сортировки на обратный
 * @param element элемент, для которого меняется порядок сортировки
 * @param orderBy текущий порядок
 */
function reverseOrderBy(element, orderBy) {
    if(orderBy === "ASC") {
        orderBy = "DESC";
    } else {
        orderBy = "ASC";
    }
    element.attr("data-order", orderBy);

}

/***
 * Удаление книги
 * @param param isn
 */
function removeBook(param) {
    if (confirm("вы действительно хотите удалить книгу" + param + "?")) {
        var msg = {};
        msg["bookISN"] = param;

        function sucess(data) {
            console.log("SUCCESS: ", data);
            location.reload();
        }
        sendData(msg, "POST", HOME + "/api/removeBook", sucess);

    } else {
        console.log("книга не удалена");
    }
}

/***
 * Удаление пользователя
 * @param name name
 */
function removeUser(name) {
    if (confirm("вы действительно хотите удалить пользователя " + name + "?")) {
        var msg = {};
        msg["name"] = name;

        function sucess(data) {
            console.log("SUCCESS: ", data["status"]);
            location.reload();
        }
        sendData(msg, "POST", HOME + "/api/removeUser", sucess);
    } else {
        console.log("пользователь не удален");
    }
}

/**
 * Установка значения isn, для того чтобы при нажатии в модальном окне save бралась информация из div.#isn который скрыт
 * @param isn
 */
function getDataBook(isn, author, bookName) {
    $("#updateBookModal").find("#isn").text(isn);
    $("#updateBookModal").find("#author").val(author);
    $("#updateBookModal").find("#bookName").val(bookName);
    removeCssClass("#updateBookModal","error");
}

function setUserNameForUpdateInfo(param) {
    var elements = ["#password"];
    $("#updateUserModal").find("#name").text(param);
    removeCssClass("#updateUserModal", elements,"error");
}

/**
 * Взять книгу
 * @param param isn
 */
function take(param) {
    var msg = {};
    msg["bookISN"] = param;

    function sucess(data) {
        console.log("take SUCCESS: ", data);
        location.reload();
    }
    sendData(msg, "POST", HOME + "/api/takeBook", sucess);
}

/**
 * Вернуть книгу
 * @param param isn
 */
function giveBack(param) {
    var msg = {};
    msg["bookISN"] = param;

    function sucess(data) {
        console.log("giveBack SUCCESS: ", data);
        location.reload();
    }
    sendData(msg, "POST", HOME + "/api/giveBackBook", sucess);

}

/**
 * генерирование html для столбца "кем взята" в зависимости от владельца
 * @param owner
 * @param isn
 * @returns {*} сгенерированный html
 */
function ownerView(owner, isn) {
    if (owner === "null") {
        return "<button onclick=\"take('" + isn +"')\">Взять</button>";
    }else if (owner === "currentUser") {
        return "<button onclick=\"giveBack('" + isn +"')\">Вернуть</button>";
    } else {
        return owner;
    }
}

/**
 * проверка соотвествия данных регулярному выражению
 */
function isValid(str) {
    var pattern = /^\S[\w\d\s]*\S$/;
    return pattern.test(str);
}

