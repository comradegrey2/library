<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%request.setCharacterEncoding("UTF-8");%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">

</head>
<body>
<div class="container">
    <h1><a href="${pageContext.request.contextPath}/books">Книги</a>&nbsp;<a href="${pageContext.request.contextPath}/users">Пользователи</a></h1>
    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal">Добавить книгу</button>
    <!-- Modal addBook -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Добавить книгу</h4>
                </div>
                <div class="modal-body">
                    <table class="table addBook">
                        <thead>
                        <tr>
                            <th>ISN</th>
                            <th>Автор</th>
                            <th>Название</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <input type="text" class="form-control" id="isn">
                            </td>
                            <td>
                                <input type="text" class="form-control" id="author">
                            </td>
                            <td>
                                <input type="text" class="form-control" id="bookName">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="modal-footer text-center">
                    <div id="status" class="hidden"></div>
                    <button type="button" class="btn btn-default" id="save">Сохранить</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal updateBook -->
    <div id="updateBookModal" class="modal fade" role="dialog">
        <div class="hidden" id="isn"></div>
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Редактировать книгу</h4>
                </div>
                <div class="modal-body">
                    <table class="table addBook">
                        <thead>
                        <tr>
                            <th>Автор</th>
                            <th>Название</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <input type="text" class="form-control" id="author">
                            </td>
                            <td>
                                <input type="text" class="form-control" id="bookName">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="modal-footer text-center">
                    <div id="status" class="hidden"></div>
                    <button type="button" class="btn btn-default" id="update">Сохранить</button>
                </div>
            </div>

        </div>
    </div>


    <table id="booksTable" class="table">
        <thead>
        <tr>
            <th>ISN</th>
            <th><a id="authorSort" onclick="sortBook('author')" class="sort-column" data-sortBy="author" data-order="ASC">Автор</a></th>
            <th><a id="bookNameSort" onclick="sortBook('bookName')" data-sortBy="bookName" data-order="ASC">Название</a></th>
            <th>Кем взята</th>
            <th>Удалить книгу</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="listValue" items="${books}">
            <tr>
                <td><a href="#updateBookModal" onclick="getDataBook('${listValue.isn}', '${listValue.author}', '${listValue.bookName}')" data-toggle="modal">${listValue.isn}</a></td>
                <td>${listValue.author}</td>
                <td>${listValue.bookName}</td>

                <c:choose>
                    <c:when test="${listValue.owner == 'null'}">
                        <td>
                            <button onClick="take('${listValue.isn}')">Взять</button>
                        </td>
                    </c:when>
                    <c:when test="${listValue.owner == 'currentUser'}">
                        <td>
                            <button onClick="giveBack('${listValue.isn}')">Вернуть</button>
                        </td>
                    </c:when>
                    <c:otherwise>
                        <td>${listValue.owner}</td>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${listValue.owner == 'null'}">
                        <td><button id="remove-${listValue.isn}" onClick="removeBook('${listValue.isn}')">Удалить</button></td>
                    </c:when>
                    <c:otherwise>
                        <td>У книги есть владелец</td>
                    </c:otherwise>
                </c:choose>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <button id="showMore">Показать еще</button>
</div>

<script src="<c:url value="/resources/js/jquery-3.2.0.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script src="<c:url value="/resources/js/script.js" />"></script>
</body>
</html>
