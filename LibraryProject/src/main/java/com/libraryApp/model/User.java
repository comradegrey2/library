package com.libraryApp.model;

/**
 * Created by captainPie on 19.03.2017.
 */
public class User {

    String name;
    String password;
    String role;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User [name=" + name + ", pass=" + password + ", role=" + role + "]";
    }
}
