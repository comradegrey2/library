package com.libraryApp.model;

/**
 * Created by captainPie on 20.03.2017.
 */
public class Book {
    String isn;
    String author;
    String bookName;
    String owner;

    public Book() {
    }

    public Book(String isn, String author, String bookName, String owner) {
        this.isn = isn;
        this.author = author;
        this.bookName = bookName;
        this.owner = owner;
    }

    public String getIsn() {
        return isn;
    }

    public void setIsn(String isn) {
        this.isn = isn;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String toJson(){
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"isn\" : \"" + this.isn + "\", ");
        sb.append("\"author\" : \"" + this.author + "\", ");
        sb.append("\"bookName\" : \"" + this.bookName + "\",");
        sb.append("\"owner\" : \"" + this.owner + "\"");
        sb.append("}");
        return sb.toString();
    }
}
