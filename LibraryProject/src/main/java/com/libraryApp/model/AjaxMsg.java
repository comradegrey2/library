package com.libraryApp.model;

/**
 * Created by captainPie on 20.03.2017.
 */
public class AjaxMsg {
    int rowLength;
    String bookISN;
    String msg;
    String orderBy;

    public int getRowLength() {
        return rowLength;
    }

    public void setRowLength(int rowLength) {
        this.rowLength = rowLength;
    }

    public String getBookISN() {
        return bookISN;
    }

    public void setBookISN(String bookISN) {
        this.bookISN = bookISN;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }
}
