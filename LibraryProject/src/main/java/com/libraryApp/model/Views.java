package com.libraryApp.model;

/**
 * Created by captainPie on 20.03.2017.
 */
public class Views {
    public static String ownerView(String authUser, String owner) {
        if (owner == null) {
            return "null";
        }else if (authUser.equals(owner)) {
            return "currentUser";
        } else {
            return owner;
        }
    }
}
