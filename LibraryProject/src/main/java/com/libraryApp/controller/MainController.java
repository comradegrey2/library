package com.libraryApp.controller;

import com.libraryApp.dao.BookDao;
import com.libraryApp.dao.UserDao;
import com.libraryApp.model.Book;
import com.libraryApp.model.User;
import com.libraryApp.model.Views;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by captainPie on 14.03.2017.
 */
@Controller
public class MainController {

    @Autowired
    UserDao userDao;

    @Autowired
    BookDao bookDao;

    @RequestMapping(value = { "/" }, method = RequestMethod.GET)
    public ModelAndView welcomePage() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ModelAndView model = new ModelAndView();
        model.addObject("message", auth.getName());
        model.setViewName("hello");
        return model;

    }

    @RequestMapping(value = "/books**", method = RequestMethod.GET)
    public ModelAndView booksPage() {
        ModelAndView model = new ModelAndView();
        model.addObject("title", "Книги");
        //получение первых пяти книг
        List<Book> list = bookDao.getBooks(0, 5, "author", "ASC");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        for (Book book: list) {
            book.setOwner(Views.ownerView(auth.getName(),book.getOwner()));
        }

        model.addObject("books", list);
        model.setViewName("books");
        return model;

    }

    @RequestMapping(value = "/users**", method = RequestMethod.GET)
    public ModelAndView usersPage() {
        ModelAndView model = new ModelAndView();
        model.addObject("title", "Пользователи");
        List<User> list = userDao.findAll();
        List<String> usersNames = new ArrayList<String>();
        for (User user: list) {
            usersNames.add(user.getName());
        }
        model.setViewName("users");
        model.addObject("users", usersNames);
        return model;

    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        HttpSession session= request.getSession(false);
        SecurityContextHolder.clearContext();
        session= request.getSession(false);
        if(session != null) {
            session.invalidate();
        }
        for(Cookie cookie : request.getCookies()) {
            cookie.setMaxAge(0);
        }
        return "redirect:/";//You can redirect wherever you want, but generally it's a good practice to show login screen again.
    }

}
