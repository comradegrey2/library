package com.libraryApp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.libraryApp.dao.BookDao;
import com.libraryApp.model.AjaxMsg;
import com.libraryApp.model.Book;
import com.libraryApp.model.Views;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by captainPie on 20.03.2017.
 */
@Controller
public class BooksController {

    @Autowired
    BookDao bookDao;

    @RequestMapping(value = "/api/saveBook", method = RequestMethod.POST,produces = "application/json; charset=utf-8")
    @ResponseBody
    String save(Book book)  throws IOException{
        String res = "";
        if (bookDao.ISNIsExist(book.getIsn())) {
            res = "книга с isn = " + book.getIsn() + " уже существует!";
        } else {
            bookDao.addBook(book);
            res = "книга успешно добавлена!";
        }
        // your logic here
        Map<String, String> map = new HashMap<String, String>();
        map.put("status", res);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(map);
    }

    @RequestMapping(value = "/api/getMore", method = RequestMethod.POST,produces = "application/json; charset=utf-8")
    @ResponseBody
    String getMore(AjaxMsg msg)  throws IOException{
        List<Book> list = bookDao.getBooks(msg.getRowLength(), msg.getRowLength() + 5, msg.getMsg(), msg.getOrderBy());

        Map<String, String> map = new HashMap<String, String>();
        List<String> books = new ArrayList<String>();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        for (Book b : list) {
            b.setOwner(Views.ownerView(auth.getName(), b.getOwner()));
            books.add(b.toJson());
        }

        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(books);
    }

    @RequestMapping(value = "/api/sortBook", method = RequestMethod.POST,produces = "application/json; charset=utf-8")
    @ResponseBody
    String sortBook(AjaxMsg msg)  throws IOException{
        List<Book> list = bookDao.getBooks(0, 5, msg.getMsg(), msg.getOrderBy());
        List<String> books = new ArrayList<String>();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        for (Book b : list) {
            b.setOwner(Views.ownerView(auth.getName(), b.getOwner()));
            books.add(b.toJson());
        }
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(books);
    }

    @RequestMapping(value = "/api/updateBook", method = RequestMethod.POST,produces = "application/json; charset=utf-8")
    @ResponseBody
    String updateBook(Book book)  throws IOException{
        bookDao.updateBook(book);
        Map<String, String> map = new HashMap<String, String>();
        map.put("status", "данные о книге обновлены!");
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(map);
    }

    @RequestMapping(value = "/api/removeBook", method = RequestMethod.POST,produces = "application/json; charset=utf-8")
    @ResponseBody
    String removeBook(AjaxMsg msg)  throws IOException{
        bookDao.removeBookByISN(msg.getBookISN());
        Map<String, String> map = new HashMap<String, String>();
        map.put("status", "ok");
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(map);
    }

    @RequestMapping(value = "/api/takeBook", method = RequestMethod.POST,produces = "application/json; charset=utf-8")
    @ResponseBody
    String takeBook(AjaxMsg msg)  throws IOException{
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        System.out.println(bookDao.getOwner(msg.getBookISN()));
        if (bookDao.getOwner(msg.getBookISN()) == null) {
            bookDao.setOwner(msg.getBookISN(), auth.getName());
        }
        return null;
    }

    @RequestMapping(value = "/api/giveBackBook", method = RequestMethod.POST,produces = "application/json; charset=utf-8")
    @ResponseBody
    String giveBackBook(AjaxMsg msg)  throws IOException{
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (bookDao.getOwner(msg.getBookISN()).equals(auth.getName())){
            bookDao.setOwner(msg.getBookISN(), null);
        }
        return null;
    }
}
