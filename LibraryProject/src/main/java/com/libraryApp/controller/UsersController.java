package com.libraryApp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.libraryApp.dao.UserDao;
import com.libraryApp.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by captainPie on 22.03.2017.
 */
@Controller
public class UsersController {

    @Autowired
    UserDao userDao;

    @RequestMapping(value = "/api/removeUser", method = RequestMethod.POST,produces = "application/json; charset=utf-8")
    @ResponseBody
    String removeUser(User user)  throws IOException {
        Map<String, String> map = new HashMap<String, String>();
        String res = "";
        if (!user.getName().equals("admin")) {
            userDao.removeUserByName(user.getName());
            res = "пользователь успешно удален!";
        } else {
            res = "пользователя admin нельзя удалять!";
        }
        map.put("status", res);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(map);
    }

    @RequestMapping(value = "/api/saveUser", method = RequestMethod.POST,produces = "application/json; charset=utf-8")
    @ResponseBody
    String saveUser(User user)  throws IOException {
        user.setRole("ROLE_ADMIN");
        Map<String, String> map = new HashMap<String, String>();
        String res = "";
        if (userDao.findByName(user.getName()) == null) {
            userDao.addUser(user);
            res = "пользователь " + user.getName() + " успешно добавлен!";
        } else {
            res = "пользователь " + user.getName() + " уже существует!";
        }
        map.put("status", res);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(map);
    }

    @RequestMapping(value = "/api/updateUser", method = RequestMethod.POST,produces = "application/json; charset=utf-8")
    @ResponseBody
    String updateUser(User user)  throws IOException{
        Map<String, String> map = new HashMap<String, String>();
        userDao.updateUserInfo(user);
        map.put("status", "пароль успешно обновлен!");
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(map);
    }
}
