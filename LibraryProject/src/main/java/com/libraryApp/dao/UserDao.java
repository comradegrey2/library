package com.libraryApp.dao;

import com.libraryApp.model.User;

import java.util.List;

/**
 * Created by captainPie on 19.03.2017.
 */
public interface UserDao {
    User findByName(String name);
    List<User> findAll();
    void removeUserByName(String name);
    void addUser(User user);
    void updateUserInfo(User user);
}
