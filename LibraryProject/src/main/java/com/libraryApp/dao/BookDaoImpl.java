package com.libraryApp.dao;

import com.libraryApp.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by captainPie on 20.03.2017.
 */
@Repository
public class BookDaoImpl implements BookDao {

    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public List<Book> getBooks(int from, int to, String column, String orderBy) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("from", from);
        params.put("to", to);
        params.put("column", column);

        String sql = "SELECT * FROM books ORDER BY " + column + " " + orderBy +" LIMIT :from, :to";

        List<Book> result = namedParameterJdbcTemplate.query(sql, params, new UserMapper());

        return result;
    }

    public boolean ISNIsExist(String val) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("val", val);

        String sql = "SELECT * FROM books WHERE isn=:val";

        List<Book> result = namedParameterJdbcTemplate.query(sql, params, new UserMapper());
        return  result.size() > 0;
    }

    public void addBook(Book book) {
        String query="insert into books values (:isn,:author,:bookName, :owner)";

        Map<String,Object> map=new HashMap<String,Object>();
        map.put("isn", book.getIsn());
        map.put("author", book.getAuthor());
        map.put("bookName", book.getBookName());
        String owner = book.getOwner().equals("null") ? null : book.getOwner();
        map.put("owner", owner);

        namedParameterJdbcTemplate.execute(query,map,new PreparedStatementCallback() {
            public Object doInPreparedStatement(PreparedStatement ps)
                    throws SQLException, DataAccessException {
                return ps.executeUpdate();
            }
        });
    }

    public void removeBookByISN(String isn) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("val", isn);
        String sql = "DELETE FROM books WHERE isn=:val";
        namedParameterJdbcTemplate.update(sql, params);
    }

    public void updateBook(Book book) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("isn", book.getIsn());
        params.put("author", book.getAuthor());
        params.put("bookName", book.getBookName());
        String sql = "UPDATE books SET author =:author, bookName=:bookName WHERE isn=:isn";
        namedParameterJdbcTemplate.update(sql, params);
    }

    public String getOwner(String isn) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("val", isn);
        String sql = "SELECT owner FROM books WHERE isn=:val";
        String name = namedParameterJdbcTemplate.queryForObject(sql,params, String.class);
        return name;
    }

    public void setOwner(String isn, String name) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("isn", isn);
        params.put("name", name);
        String sql = "UPDATE books SET owner =:name WHERE isn =:isn";
        namedParameterJdbcTemplate.update(sql, params);
    }


    private static final class UserMapper implements RowMapper<Book> {

        public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
            Book book = new Book(rs.getString("isn"), rs.getString("author"), rs.getString("bookName"), rs.getString("owner"));
            return book;
        }
    }
}
