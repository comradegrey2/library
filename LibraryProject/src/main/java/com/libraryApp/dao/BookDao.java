package com.libraryApp.dao;

import com.libraryApp.model.Book;

import java.util.List;

/**
 * Created by captainPie on 20.03.2017.
 */
public interface BookDao {
    List<Book> getBooks(int from, int to, String column, String orderBy);
    boolean ISNIsExist(String val);
    void addBook(Book book);
    void removeBookByISN(String isn);
    void updateBook(Book book);
    String getOwner(String isn);
    void setOwner(String isn, String name);
}
