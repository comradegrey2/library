package com.libraryApp.dao;

import com.libraryApp.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by captainPie on 19.03.2017.
 */
@Repository
public class UserDaoImpl implements UserDao{

    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public User findByName(String name) {

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", name);

        String sql = "SELECT * FROM users WHERE name=:name";
        User result = null;
        try {
            result = namedParameterJdbcTemplate.queryForObject(
                    sql,
                    params,
                    new UserMapper());
        } catch (EmptyResultDataAccessException e) {
            return result;
        }

        return result;

    }

    public List<User> findAll() {

        Map<String, Object> params = new HashMap<String, Object>();

        String sql = "SELECT * FROM users ORDER BY name";

        List<User> result = namedParameterJdbcTemplate.query(sql, params, new UserMapper());

        return result;

    }

    public void removeUserByName(String name) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("val", name);
        String sql = "DELETE FROM users WHERE name=:val";
        namedParameterJdbcTemplate.update(sql, params);
    }

    public void addUser(User user) {
        String query="insert into users values (:name,:password, :role)";

        Map<String,Object> map=new HashMap<String,Object>();
        map.put("name", user.getName());
        map.put("password", user.getPassword());
        map.put("role", user.getRole());

        namedParameterJdbcTemplate.execute(query,map,new PreparedStatementCallback() {
            public Object doInPreparedStatement(PreparedStatement ps)
                    throws SQLException, DataAccessException {
                return ps.executeUpdate();
            }
        });
    }

    public void updateUserInfo(User user) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", user.getName());
        params.put("password", user.getPassword());
        String sql = "UPDATE users SET password =:password WHERE name=:name";
        namedParameterJdbcTemplate.update(sql, params);
    }

    private static final class UserMapper implements RowMapper<User> {

        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            user.setName(rs.getString("name"));
            user.setPassword(rs.getString("password"));
            user.setRole(rs.getString("role"));
            return user;
        }
    }
}
