package com.libraryApp.services;

import com.libraryApp.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.security.core.userdetails.User;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by captainPie on 19.03.2017.
 */

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UserDao userDao;

    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        // указываем роли для этого пользователя
        Set<GrantedAuthority> roles = new HashSet();
        com.libraryApp.model.User user = userDao.findByName(name);
        roles.add(new SimpleGrantedAuthority(user.getRole()));
        UserDetails userDetails = new User(user.getName(), user.getPassword(), roles);
        return userDetails;
    }
}
