<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%request.setCharacterEncoding("UTF-8");%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">

</head>
<body>

    <!-- Modal addUser -->
    <div id="addUserModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Добавить пользователя</h4>
                </div>
                <div class="modal-body">
                    <table class="table addBook">
                        <thead>
                        <tr>
                            <th>Имя</th>
                            <th>Пароль</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <input type="text" class="form-control" id="name">
                            </td>
                            <td>
                                <input type="text" class="form-control" id="password">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="modal-footer text-center">
                    <div id="status" class="hidden"></div>
                    <button type="button" class="btn btn-default" id="save">Сохранить</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal updateUser -->
    <div id="updateUserModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Редактировать данные пользователя</h4>
                </div>
                <div class="modal-body">
                    <table class="table addBook">
                        <thead>
                        <tr>
                            <th>Имя</th>
                            <th>Новый пароль</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <label id="name">Name</label>
                            </td>
                            <td>
                                <input type="text" class="form-control" id="password">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="modal-footer text-center">
                    <div id="status" class="hidden"></div>
                    <button type="button" class="btn btn-default" id="update">Сохранить</button>
                </div>
            </div>

        </div>
    </div>

    <div class="container">
        <h1><a href="${pageContext.request.contextPath}/books">Книги</a>&nbsp;<a href="${pageContext.request.contextPath}/users">Пользователи</a></h1>
        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#addUserModal">Добавить пользователя</button>

        <table id="usersTable" class="table">
            <thead>
            <tr>
                <th>Имя пользователя</th>
                <th>Удалить пользователя</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="listValue" items="${users}">
                <tr>
                    <td><a data-toggle="modal" onclick="setUserNameForUpdateInfo('${listValue}')" data-target="#updateUserModal">${listValue}</a></td>
                    <td><button onClick="removeUser('${listValue}')">Удалить</button></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</body>

<script src="<c:url value="/resources/js/jquery-3.2.0.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script src="<c:url value="/resources/js/script.js" />"></script>
</html>
