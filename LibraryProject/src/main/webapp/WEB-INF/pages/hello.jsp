<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%request.setCharacterEncoding("UTF-8");%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
</head>
<body>
<div class="container">
    <h1>Библиотека</h1>

    <c:choose>
        <c:when test="${pageContext.request.userPrincipal.authenticated}">
            <h1>Hello : ${message}!</h1><span><a href="${pageContext.request.contextPath}/logout">Выйти</a></span>
            <a href="${pageContext.request.contextPath}/books">Книги</a>
            <a href="${pageContext.request.contextPath}/users">Пользователи</a><br>
        </c:when>
        <c:otherwise>
            <h1>login: admin, password: 111</h1>
            <a href="${pageContext.request.contextPath}/login">Авторизация</a>
        </c:otherwise>
    </c:choose>
</div>

</body>
</html>
